<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Riddle extends Model
{
    protected $casts = [
    	'hints' => 'json',
    ];
}
