<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('riddles', 'RiddleController');

Route::get('riddles/{riddle}/mark', function () {

	notify()->flash('Correct!', 'success', [
		'timer' => 2000,
	]);

	return back();
});

Route::post('riddles/{riddle}/mark', function (App\Models\Riddle $riddle) {
	return request()->all();
})->name('mark');
