@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title text-center">Riddles</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3">
                                @if($riddles->count())
                                    <table class="table table-bordered">
                                        <tbody>
                                            @foreach($riddles->chunk(4) as $riddlesSet)
                                                <tr>
                                                    @foreach($riddlesSet as $riddle)
                                                        <td>
                                                            <a href="{{ route('riddles.show', $riddle) }}">
                                                                {{ $riddle->id }}
                                                            </a>
                                                        </td>
                                                    @endforeach
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                @else
                                    <p class="text-center">No Riddle</p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
