@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title text-center">{{ $riddle->id }}</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-10 col-sm-offset-1">
                                <p>
                                    {{ $riddle->text }}
                                </p>
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <tbody>
                                            <tr>
                                                @for($i = 0; $i < strlen($riddle->answer); $i++)
                                                    <td>
                                                        <input type="text" id="answer{{ $i }}" class="form-control uppercase" maxlength="1">
                                                    </td>
                                                @endfor
                                            </tr>
                                        </tbody>
                                    </table>                                    
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <tbody>
                                            <tr>
                                                @foreach($riddle->hints as $hint)
                                                    <td class="text-center">
                                                        {{ $hint }}
                                                    </td>
                                                @endforeach
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-xs-3">
                                <a href="#" class="btn btn-primary btn-block">Discuss</a>
                            </div>
                            <div class="col-xs-3">
                                <a href="#" class="btn btn-primary btn-block">Unlock</a>
                            </div>
                            <div class="col-xs-3">
                                <a href="#" class="btn btn-primary btn-block">Hint</a>
                            </div>
                            <div class="col-xs-3 text-right">
                                <button type="button" id="nextBtn" class="btn btn-primary">Next</button>
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script>
        $(document).ready(function () {


            $('#nextBtn').click(function () {

                swal({
                  title: "Is this your final answer?",
                  text: "Click Yes to continue",
                  type: "info",
                  confirmButtonText: "Yes",
                  cancelButtonText: "No",
                  showCancelButton: true,
                  closeOnConfirm: false,
                  showLoaderOnConfirm: true
                }, function () {

                // get riddle id
                function getRiddleId() {
                    return location.pathname.split('/')[2];
                }

                    // get riddle
                    function getRiddleAnswer(id) {
                        axios.get('/api/riddles/' + id)
                            .then(function (response) {

                                // Get inputs
                                var inputs = [];
                                $(':text').each(function()  {
                                    inputs.push($(this).val());
                                });

                                if (inputs.join('').toLowerCase() != response.data.answer.toLowerCase()) {

                                    swal({
                                        title: "Wrong",
                                        text: "Please try again",
                                        type: "error",
                                    });
                                }else{
                                    swal({
                                        title: "Good job!", text: "Correct Answer. You got 10 points.", type: "success"
                                    }, 
                                    function () {
                                        window.location.replace(window.location.origin + '/riddles/' + (parseInt(getRiddleId()) + parseInt(1)));
                                    });

                                }

                            })
                            .catch(function (error) {
                                console.log(error);
                            });
                    }

                    getRiddleAnswer(getRiddleId()); 

                });                            

            });
        });

    </script>
    <script>
        @if (notify()->ready())
            swal({
                title: "{!! notify()->message() !!}",
                type: "{!! notify()->type() !!}",
                @if (notify()->option('timer'))
                    timer: {{ notify()->option('timer') }}
                @endif
            });
        @endif
    </script>
@endpush

