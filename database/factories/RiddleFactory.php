<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Riddle::class, function (Faker $faker) {
	$answer = strtoupper($faker->word);
	$garbage = strtoupper($faker->word);

	for($i = 0; $i < strlen($answer); $i++) {
		$letters[] = $answer[$i];
	}

	for($i = 0; $i < strlen($garbage); $i++) {
		$letters[] = $garbage[$i];
	}

	shuffle($letters);

    return [
        'text' => $faker->sentence,
        'answer' => $answer,
        'hints' => $letters,
    ];
});
